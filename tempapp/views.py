from django.shortcuts import render
from requests import *
# Create your views here.
def home(req):
    try:
        if req.POST.get("city"):
            city = str(req.POST.get("city"))
            url = "http://api.openweathermap.org/data/2.5/forecast?q="+city+"&units=metric&appid=5b4b7aac1d593cfae4a3ebd879676293"
            res = get(url)
            data = res.json()
            # print(data)
            temp = data['list'][0]['main']['temp']
            print(temp)
            msg = "Temperature of "+city+" is "+str(temp)
            print(city)
            return render(req,"home.html",{"msg":msg})
    except Exception as e:
        msg = "No such city"
        print(e)
        return render(req,"home.html",{"msg":msg})
    return render(req,"home.html")